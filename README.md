Offline news search program
===========================

This program is released in the public domain by /u/ZugNachPankow.

##Prerequisites

 - `nodejs`
 - `wget`

###Node on Win/Mac OS X

To install node, download the appropriate installer from http://nodejs.org/download/ and execute it (on Windows, the .msi file; on Mac, the .pkg file)

###Node on Linux

To install node, install `nodejs` through your package manager of choice (eg. `sudo apt-get install nodejs`).

##Wget on Windows

To install Wget for Windows, download the setup file from here: http://gnuwin32.sourceforge.net/packages/wget.htm

##Wget on Mac

Wget is already installed on Mac.

##Wget on Linux

Wget should be already installed on most Linux distributions. If it isn't, download it with your package installer of choice (eg. `sudo apt-get install wget`).

##Running the server

To run the server, navigate to the folder where the project is stored, and run `nodejs server.js`.

##Getting started

To issue commands, go to http://127.0.0.1:9090/ . The Control Panel for the program will appear. You can search for articles, update the news feeds, purge old articles, or shut down the application.
>The first thing you should do as soon as you install the program is update the feeds. The server will connect to the RSS feeds specified in `source.json`, download the list of articles, index it (make it searchable), and copy each article to your computer.
>Copying articles to your computer takes quite some time, especially the first time. Local copies may not be immediately available.

##Searching

To search articles, click on "Search articles" in the Control Panel. Enter the search terms in the box: results will appear automatically.
>The server application must be running for search results to be displayed.

A list of articles matching your search terms will appear. By default, it is sorted by relevance; you may sort it by title or date, clicking on the labels on the table. Each title is clickable, and it is a link to the local copy of the article.
>It may take a while for local copies to be created after a feed refresh.

##Refreshing feeds

To refresh feeds, click on "Refresh feeds" in the Control Panel. The server will check for new articles, download them to your hard drive, and make them searchable.
>You can control the sources by editing `sources.json`.

##Purge old articles

To purge old articles, click on "Article cleanup" in the Control panel. Enter a number of days: articles published more than X days ago will be removed.

##Shutdown

To shutdown the server application, either use Ctrl+C in the terminal window, or click "Shutdown" in the Control Panel. The server will save data to disk, and then shut down cleanly.