var http = require('http'),
	fs   = require('fs'),
	lunr = require('./lunr.js'),
	feed = require("feed-read"),
	async = require("async"),
	exec = require('child_process').exec;

var isFirstRun = !fs.existsSync('data.json');

metadata = {}
log = ""

if (isFirstRun) {
	console.log("Generating an empty archive...");
	var index = lunr(function () {
	    this.field('title', {boost: 10})
	    this.field('body')
	    this.ref('id')
	});
	index.on('add', function(doc, index) {
		metadata[doc.id] = doc.title;
	})

	fs.writeFileSync('data.json', JSON.stringify(index.toJSON()));
	fs.writeFileSync('metadata.json', JSON.stringify(metadata));
} else {
	console.log("Loading the existing archive...");
	index = lunr.Index.load(JSON.parse(fs.readFileSync("data.json")));
	metadata = JSON.parse(fs.readFileSync("metadata.json"));
}
console.log("Done!");

var server = http.createServer(function (req, res) {
	console.log(req.method, req.url);
	req = require('url').parse(req.url, true);
	res.writeHead(200, {'Content-Type': 'text/html'});
	if (req.pathname == "/search.js") {
		results = index.search(req.query.term);
		resultList = [];
		results.forEach(function (result) {
			resultList.push({
				"link": metadata["L" + result.ref],
				"title": metadata["T" + result.ref],
				"published": metadata["P" + result.ref],
				"source": metadata["S" + result.ref],
				"score": result.score
			});
		})
		res.end(JSON.stringify(resultList));
	} else if (req.pathname == '/cleanup.htm') {
		today = new Date();
		if (req.query.age) {
			for (var key in metadata) {
				if (metadata.hasOwnProperty(key)) {
					if (key.slice(0, 1) == "P") {
						date = new Date(metadata[key]);
						var oneDay = 24*60*60*1000;
						diff = Math.round(Math.abs((today.getTime() - date.getTime())/(oneDay)))
						if (diff > req.query.age) {
							id=key.slice(1);
							console.log("Removing document", id);
							index.remove({"ref": id});
							delete metadata["P" + id]
							delete metadata["L" + id]
							delete metadata["S" + id]
							delete metadata["T" + id]
						}
					}
				}
			}
		}
		res.end(fs.readFileSync('cleanup.htm'));
	} else if (req.pathname == '/sources.htm') {
		if (req.query.action == 'del') {
			sources = JSON.parse(fs.readFileSync("sources.json"));
			sources.forEach(function (source) {
				if (source.name == new Buffer(req.query.name, 'base64').toString('utf8')) {
					sources.splice(sources.indexOf(source), 1);
				}
			})
			fs.writeFileSync("sources.json", JSON.stringify(sources));
			res.write("Source deleted!");
		} else if (req.query.action == 'add') {
			sources = JSON.parse(fs.readFileSync("sources.json"));
			sources.push({"name": req.query.name, "link": req.query.link});
			fs.writeFileSync("sources.json", JSON.stringify(sources));
			res.write("Source added!")
		}
		res.write("<h1>Source management</h1><form method='get' action='sources.htm'><table><tr><th>Source</th><th>Address</th></tr>");
		JSON.parse(fs.readFileSync("sources.json")).forEach(function (source) {
			res.write("<tr><td>" + source.name + "</td><td><code>" + source.link + "</code></td><td><a href='/sources.htm?action=del&name=" + new Buffer(source.name).toString('base64') + "'>Delete source</a></tr>");
		})
		res.write("<tr><td><input type='text' name='name' placeholder='Source name' /></td><td><input type='text' name='link' placeholder='Feed address' /></td><td><input type='hidden' name='action' value='add' /><input type='submit' value='Add source' /></td></tr>");
		res.end("</table></form>");
	} else if (req.pathname == '/refresh.htm') {
		refreshFeeds();
		res.end(fs.readFileSync("refresh.htm"));
	} else if (req.pathname == '/shutdown.htm') {
		res.end("Shutting down!");
		save();
		process.exit();
	} else if (req.pathname == '/') {
		res.end(fs.readFileSync('welcome.htm'));
	} else {
		if (fs.existsSync('.' + req.pathname)) {
			res.end(fs.readFileSync('.' + req.pathname));
		} else {
			res.end("");
		}
	}
})
 
server.listen(9090);
 
console.log('Server running at http://127.0.0.1:9090/');

function refreshFeeds() {
	feedTaskList = [];
	log += "Refreshing feeds now.\n";
	JSON.parse(fs.readFileSync("sources.json")).forEach(function(source) {
		console.log("Refreshing source", source.name, "(" + source.link + ")...");
		log += "Refreshing source " + source.name + " at " + source.link + "\n";
		feedTaskList.push(function (callback) {
			feed(source.link, function(err, articles) {
				taskList = [];
				if (err) {
					log += "Error while parsing source '" + source.name + "': " + err + "\n";
					console.log("Error while parsing source '" + source.name + "':", err);
				} else {
					console.log(source.name, "loaded!");
					log += "Source " + source.name + "loaded! Articles:\n";
					log += JSON.stringify(articles) + "\n";
					articles.forEach(function (article) {
						log += "Dealing with article: " + JSON.stringify(article) + "\n";
						article.link=article.link.replace(/\?.+/, "").replace(/#.+/, "");
						id = (article.title + article.link).hashCode();
						if (metadata["T" + id]) {
							log += "An article with the ID " + id + " already exists.\n";
						} else {
							metadata["S" + id] = source.name;
							metadata["T" + id] = article.title;
							metadata["L" + id] = article.link.replace(/https?:\//, "");
							metadata["P" + id] = article.published;
							log += "Pushing the mirroring of this article.";
							taskList.push(function (theCallback) {
								console.log("Mirroring", article.link);
								command = "wget --reject png,jpg,jpeg,gif --tries=3 --no-check-certificate -H -k -K -p \"" + article.link + "\"";
								log += "Executing command " + command + "\n";
								exec(command, function() {
									log += "Command " + command + " executed. Args:" + JSON.stringify(arguments) + "\n";
									console.log("Mirrored", article.link)
									theCallback(null);
								});
							})
							index.add({
								id: id,
								title: article.title,
								body: article.content
							});
						}
					});
				}
				callback(err, taskList);
				// Each article has the following properties:
				// 
				//   * "title"     - The article title (String).
				//   * "author"    - The author's name (String).
				//   * "link"      - The original article link (String).
				//   * "content"   - The HTML content of the article (String).
				//   * "published" - The date that the article was published (Date).
				//   * "feed"      - {name, source, link}
				// 
			});
		})
	});
	async.series(feedTaskList, function (err, wgetTaskListList) {
		async.eachSeries(wgetTaskListList, function (wgetTaskList, callback) {
			async.series(wgetTaskList, callback);
		}, function() {
			console.log("Done mirroring a source!");
			save();
		})
	})
}

function save() {
	fs.writeFileSync("data.json", JSON.stringify(index.toJSON()));
	fs.writeFileSync("metadata.json", JSON.stringify(metadata));
}

String.prototype.hashCode = function() {
  var hash = 0, i, chr, len;
  if (this.length == 0) return hash;
  for (i = 0, len = this.length; i < len; i++) {
    chr   = this.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
};

process.on('SIGINT', function() {
	console.log("\nSaving...");
	save();
	fs.writeFileSync("logfile", log);
	console.log("Saved! Exiting.")
	process.exit();
});